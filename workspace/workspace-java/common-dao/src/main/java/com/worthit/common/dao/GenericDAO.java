package com.worthit.common.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManagerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * Interface to set the contract for DAO facilities.
 * @author Delicia
 */
@Transactional(rollbackFor = Exception.class)
public interface GenericDAO<T, ID extends Serializable> {

    /**
     * Get the Class of the entity.
     *
     * @return the class
     */
    Class<T> getEntityClass();

    /**
     * Find an entity by its primary key
     *
     * @param id the primary key
     * @return the entity
     */
    T findById(final ID id);

    /**
     * Load all entities.
     *
     * @return the list of entities
     */
    List<T> findAll();

    /**
     * Find entities based on an example.
     *
     * @param exampleInstance the example
     * @return the list of entities
     */
    List<T> findByExample(final T exampleInstance);

    /**
     * Find entities based on an example and applies "like" to string searches.
     *
     * @param exampleInstance the example
     * @return the list of entities
     */
    List<T> findByExampleWithLike(final T exampleInstance);

    /**
     * Find using a named query.
     *
     * @param queryName the name of the query
     * @param params the query parameters
     *
     * @return the list of entities
     */
    List<T> findByNamedQuery(
            final String queryName,
            Object... params);

    /**
     * Find using a named query.
     *
     * @param queryName the name of the query
     * @param params the query parameters
     *
     * @return the list of entities
     */
    List<T> findByNamedQueryAndNamedParams(
            final String queryName,
            final Map<String, ? extends Object> params);

    /**
     * Count all entities.
     *
     * @return the number of entities
     */
    int countAll();

    /**
     * Count entities based on an example.
     *
     * @param exampleInstance the search criteria
     * @return the number of entities
     */
    int countByExample(final T exampleInstance);

    /**
     * save an entity. This can be either a INSERT or UPDATE in the database.
     *
     * @param entity the entity to save
     *
     * @return the saved entity
     */
    T save(final T entity);

    /**
     * save a list of entities. This can be either a INSERT or UPDATE in the database.
     *
     * @param entity the entity to save
     *
     * @return the saved entity
     */
    List<T> save(List<T> entities);

    /**
     * delete an entity from the database.
     *
     * @param entity the entity to delete
     */
    void delete(final T entity);

    /**
     * delete an entity from the database by example.
     *
     * @param entity the entity to delete
     */
    void deleteByExample(final T exampleInstance);

    /**
     * delete all entity from the database.
     *
     * @param entity the entity to delete
     */
    void deleteAll();

    /**
     * 
     * @param entityManagerFactory 
     */
    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory);
}//of GenericDAO 
