package com.worthit.common.dao;

import com.worthit.common.dao.logic.GenericDAOLogic;

/**
 * DAO Factory used for creating creating class typed Generic DAOs.
 * @author delicia.brummitt
 */
public final class DAOFactory {

    private DAOFactory() {
        //prevent anyone from calling new on DAO Factory
    }

    /**
     * Factory Method that creates a DAO based on the Class Type passed. 
     * @param persistentClass class indication that type of DAO required
     * @return DAO typed for the <code>persistentClass</code>
     */
    public static GenericDAO getGenericDAO(final Class persistentClass) {
        return new GenericDAOLogic(persistentClass);

    }//of getGenericDAO
}//of class

