package com.worthit.common.dao.logic;

import com.worthit.common.dao.GenericDAO;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

/**
 * @author Jurgen Lust original author via Belgium Java Users Group 
 * @author Delicia Brummitt Re-factored with additional functionality 
 * @see http://www.bejug.org/confluenceBeJUG/display/BeJUG/Generic+DAO+example?decorator=printable
 */
public class GenericDAOLogic<T, ID extends Serializable>
        extends HibernateDaoSupport
        implements GenericDAO<T, ID> {

    private final Class<T> persistentClass_;
    EntityManagerFactory entityManagerFactory_ = null;
    @PersistenceContext
    protected EntityManager entityManager_;
    protected Session session_;

    /**
     * Default Constructor
     */
    @SuppressWarnings("unchecked")
    public GenericDAOLogic() {
        persistentClass_ = (Class<T>) ((ParameterizedType) 
                getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        initSession();
    }

    /**
     * Constructor initialized using the paramaterized type.
     * @param persistentClass 
     */
    public GenericDAOLogic(final Class<T> persistentClass) {
        super();
        persistentClass_ = persistentClass;
        initSession();
    }

    /**
     * Initializes the Session
     */
    private void initSession() {
        session_ = (Session) getEntityManager().getDelegate();
    }

    /**
     * 
     * @return 
     */
    @Override
    public int countAll() {
        return countByCriteria();
    }

    /**
     * 
     * @param exampleInstance
     * @return 
     */
    @Override
    public int countByExample(final T exampleInstance) {

        Criteria crit = session_.createCriteria(getEntityClass());
        crit.setProjection(Projections.rowCount());
        crit.add(Example.create(exampleInstance));

        return (Integer) crit.list().get(0);
    }

    /**
     * 
     * @return 
     */
    @Override
    public List<T> findAll() {
        return findByCriteria();
    }

    /**
     * 
     * @param exampleInstance
     * @return 
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<T> findByExample(final T exampleInstance) {

        final Criteria crit = session_.createCriteria(getEntityClass())
                .add(Example.create(exampleInstance));
        final List<T> result = crit.list();
        return result;
    }

    /**
     * Find entities based on an example and applies "like" to string searches.
     *
     * @param exampleInstance the example
     * @return the list of entities
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<T> findByExampleWithLike(final T exampleInstance) {

        final Criteria crit = session_.createCriteria(getEntityClass())
                .add(Example.create(exampleInstance)
                            .ignoreCase()
                            .enableLike(MatchMode.ANYWHERE));
        final List<T> result = crit.list();
        return result;
    }

    /**
     * 
     * @param id
     * @return 
     */
    @Override
    public T findById(final ID id) {
        final T result = getEntityManager().find(persistentClass_, id);
        return result;
    }

    /**
     * 
     * @param name
     * @param params
     * @return 
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<T> findByNamedQuery(final String name, final Object... params) {
        javax.persistence.Query query = getEntityManager().createNamedQuery(
                name);

        for (int i = 0; i < params.length; i++) {
            query.setParameter(i + 1, params[i]);
        }

        final List<T> result = (List<T>) query.getResultList();
        return result;
    }

    /**
     * 
     * @param name
     * @param params
     * @return 
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<T> findByNamedQueryAndNamedParams(final String name,
            final Map<String, ? extends Object> params) {
        javax.persistence.Query query = getEntityManager().createNamedQuery(
                name);

        for (final Map.Entry<String, ? extends Object> param : params.entrySet()) {
            query.setParameter(param.getKey(), param.getValue());
        }

        final List<T> result = (List<T>) query.getResultList();
        return result;
    }

    /**
     * 
     * @return 
     */
    @Override
    public Class<T> getEntityClass() {
        return persistentClass_;
    }

    /**
     * 
     * @param entityManager 
     */
    @PersistenceContext
    public void setEntityManager(final EntityManager entityManager) {
        entityManager_ = entityManager;
    }

    /**
     * 
     * @return 
     */
    public EntityManager getEntityManager() {
        return entityManager_;
    }

    /**
     * Use this inside subclasses as a convenience method.
     */
    protected List<T> findByCriteria(final Criterion... criterion) {
        return findByCriteria(-1, -1, criterion);
    }

    /**
     * Use this inside subclasses as a convenience method.
     */
    @SuppressWarnings("unchecked")
    protected List<T> findByCriteria(final int firstResult,
            final int maxResults, final Criterion... criterion) {

        Criteria crit = session_.createCriteria(getEntityClass());

        for (final Criterion c : criterion) {
            crit.add(c);
        }

        if (firstResult > 0) {
            crit.setFirstResult(firstResult);
        }

        if (maxResults > 0) {
            crit.setMaxResults(maxResults);
        }

        final List<T> result = crit.list();
        return result;
    }

    protected int countByCriteria(final Criterion... criterion) {

        Criteria crit = session_.createCriteria(getEntityClass());
        crit.setProjection(Projections.rowCount());

        for (final Criterion c : criterion) {
            crit.add(c);
        }

        return (Integer) crit.list().get(0);
    }

    /**
     * 
     */
    @Override
    public void delete(final T entity) {
        getEntityManager().remove(entity);
    }

    /**
     * 
     */
    @Override
    public void deleteByExample(final T exampleInstance) {
        List<T> deletes = findByExample(exampleInstance);
        for (T entity : deletes) {
            getEntityManager().remove(entity);
        }
    }

    /**
     * Delete everything in the table.  Use with EXTREME caution.
     */
    @Override
    public void deleteAll() {
        List<T> entities = findAll();
        for (T entity : entities) {
            delete(entity);
        }
    }

    /**
     * Save an entity if it does not already exist, merge the entity if it does 
     * not.
     * @param entity
     * @return 
     */
    @Override
    public T save(final T entity) {
        List<T> found = findByExample(entity);
        T savedEntity = null;
        if (found == null || found.isEmpty()) {//if this is brand new w/ nothing else like
            //session_.beginTransaction();
            savedEntity = (T) session_.save(entity);
            session_.flush();
            session_.refresh(savedEntity);
            //getEntityManager().getTransaction().commit();           
        } else {
            //session_.beginTransaction();
            savedEntity = (T) session_.merge(entity);
            //savedEntity = getEntityManager().merge(entity);
            //getEntityManager().getTransaction().commit();
        }
        return savedEntity;
    }

    /**
     * 
     * @param entities
     * @return 
     */
    @Override
    public List<T> save(final List<T> entities) {
        List<T> saved = new ArrayList<T>();
        for (T entity : entities) {
            saved.add(save(entity));
        }
        return saved;
    }

    /**
     * 
     * @param entityManagerFactory 
     */
    @Override
    public void setEntityManagerFactory(
            EntityManagerFactory entityManagerFactory) {
        entityManagerFactory_ = entityManagerFactory;

    }
}
